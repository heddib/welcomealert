# README #

WelcomeAlert is Cydia Tweak which display a message when you unlock your device.
Available on my Cydia repo: http://cydia.heddibeuuuh.tk

### How do I get set up? ###

* Summary of set up

         You need a jailbroken iPhone / iPad
         Tested on iOS 9.1

* Configuration

         Clone this git

* Dependencies

         MobileSubstrate, PreferenceLoader

* How to run tests

         In MobileTerminal run the following command: 'make package install'
         Respring and go in the settings app to find the preference bundle called 'WelcomeAlert'.

* Deployment instructions

         In MobileTerminal run the following command: 'make package' and you will get a deb package.