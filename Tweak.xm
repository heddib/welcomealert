#import "UIKit/UIKit.h"
#define settingsPath [NSHomeDirectory() stringByAppendingPathComponent:@"/Library/Preferences/fr.heddib.welcomealert.plist"]

%hook SBDeviceLockController

- (BOOL)attemptDeviceUnlockWithPassword:(NSString *)passcode appRequested:(BOOL)requested {

NSMutableDictionary *prefs = [[NSMutableDictionary alloc] initWithContentsOfFile:settingsPath];

	BOOL isEnabled = [[prefs objectForKey:@"enabled"] boolValue];
	BOOL isSuccessful = %orig;

	if(isSuccessful && isEnabled){
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bienvenue !" message:[NSString stringWithFormat:@"Bienvenue bb :3"] delegate:nil cancelButtonTitle:@"Merchiii :3" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
	[prefs release];

	return isSuccessful;

}

%end