#import <Preferences/Preferences.h>

@interface WelcomeAlertListController: PSListController {
}
@end

@implementation WelcomeAlertListController
- (id)specifiers {
	if(_specifiers == nil) {
		_specifiers = [[self loadSpecifiersFromPlistName:@"WelcomeAlert" target:self] retain];
	}
	return _specifiers;
}
@end

// vim:ft=objc
